# ezcfn

A bootstrap for vanilla CloudFormation templating.

*ezcfn* is a CloudFormation bundling tool for keeping CloudFormation workflows
vanilla-flavoured.

*ezcfn* simplifies working with vanilla CloudFormation templates. It takes care
of building, gathering and distributing resource artifacts (such as
`Lambda Function` code, or `APIGateway` specifications), without altering
the development workflow when working with static interchange format
(*json* and *yaml*) templates.

*ezcfn* dissects and extends the existing functionalities of tools such as the
AWS CLI `cloudformation package` command.

Repository: [https://bitbucket.org/victorykit/ezcfn](https://bitbucket.org/victorykit/ezcfn)

Repository Mirror: [https://github.com/victoryk-it/ezcfn](https://github.com/victoryk-it/ezcfn)

## Get Started

Let’s take a simple example of a CloudFormation Template with a Lambda function.

```
AWSTemplateFormatVersion: '2010-09-09'
Resources:
  DockerBackedLambdaFunction:
    Type: AWS::Lambda::Function
    Properties:
      Handler: index.handler
      Code: docker:./?buildargs[with-joy]=true
      Runtime: nodejs14.x
  PyPaBackedLambdaFunction:
    Type: AWS::Lambda::Function
    Properties:
      Handler: index.handler
      Code: pypa:./python-pip-build-lambda/
      Runtime: python3.9
  DirectoryBackedLambdaFunction:
    Type: AWS::Lambda::Function
    Properties:
      Handler: index.handler
      Code: zip:./python-lambda-folder/
      Runtime: python3.9
```

You can now specify the `Code` property as `file`, `zip`, `docker`,
`http`, `pip`, `npm`, or `script` URI string. `ezcfn` will take care
of building the specified artifact.

The example assumes that there is a `Dockerfile` at the same directory
present as the template. The generated Docker Image is responsible for building
the Lambda artifact, and provide the results as a `ezcfn.zip` at the root of
the image. `ezcfn` will then resolve the template’s and the artifact’s path
to an output folder, which can then be uploaded to S3.

You can then execute the following command and find everything bundled under
`ezcfn.out/`.

```
python3 -m ezcfn resolve my-template.yaml --s3-bucket mybucket --s3-prefix myprefix
```

## Documentation

The documentation can be found under [https://victorykit.bitbucket.io/ezcfn/](https://victorykit.bitbucket.io/ezcfn/).

## Licensing

Copyright (C) 2021  Tiara Rodney (victoryk.it)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <[https://www.gnu.org/licenses/](https://www.gnu.org/licenses/)>.

## More Information


* Architecture


* Contribution Guidelines
