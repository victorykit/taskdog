#!/usr/bin/env python3
import configparser, argparse, os


def parse_args():

    parser = argparse.ArgumentParser( 'bump_version', description=__doc__, formatter_class=argparse.RawTextHelpFormatter )

    parser.add_argument(
        'type',
        choices=['major', 'minor', 'patch', 'pre-release' ],
        default = os.getcwd()
    )
    
    parser.add_argument(
        '--alpha',
        action='store_true',
    )
    
    parser.add_argument(
        '--beta',
        action='store_true',
    )
    
    parser.add_argument(
        '--rc', '--release-candidate',
        action='store_true',
    )

    args = vars( parser.parse_args() )

    if [ args['alpha'], args['beta'], args[ 'rc' ] ].count( True ) > 1:

        raise Exception( 'More than one pre-release identifier specified')

    return args



def parse_version_from_string( string ):

    verstr = string

    prv = None
    prtype = None
    if '-' in string:

        verstr, prtype = string.split( '-', 1 )
        psegs = prtype.split( '.', 1 )
        if len( psegs ) > 1:

            prtype, prv = psegs
            if not str.isdigit( prv ):

                raise Exception( 'invalid pre-release version' )

            prv = int( prv )

        if prtype not in [ 'alpha', 'beta', 'rc' ]:

            raise Exception( 'invalid pre-release identifier' )

    version_segments = verstr.split( '.' )
    if len( version_segments ) < 3: raise Exception( 'invalid semver struct' )

    majorv, minorv, patchv = version_segments

    if False in [ str.isdigit( v ) for v in [ majorv, minorv, patchv ] ]:

        raise Exception( 'invalid semver struct' )

    return int(majorv), int(minorv), int(patchv), prtype, prv


config = configparser.ConfigParser()

config.read( 'setup.cfg' )

version_string = config[ 'metadata' ][ 'version' ]

args = parse_args()

majorv, minorv, patchv, prtype, prv = parse_version_from_string( version_string )


if args['type'] == 'major':
    majorv = majorv + 1
    minorv = 0
    patchv = 0
    prv = None
elif args['type'] == 'minor':
    minorv = minorv + 1
    patchv = 0
    prv = None
elif args['type'] == 'patch':
    patchv = patchv + 1
    prv = None
elif args['type'] == 'pre-release':
    if prtype == None:

        raise Exception( 'No pre-release identifier present' )

    elif True not in [ args['alpha'], args['beta'], args[ 'rc' ] ]:

        raise Exception( 'No pre-release identifier specified' )


    if prv == None: prv = 1
    else: prv = prv + 1

new_string = '.'.join( [ str(v) for v in (majorv, minorv, patchv)] )

prsel = None
if args['alpha']:
    prsel = 'alpha'
    new_string += '-alpha'
elif args['beta']:
    prsel = 'beta'
    new_string += '-beta'
elif args['rc']:
    prsel = 'rc'
    new_string += '-rc'



if prv != None:

    if prsel == None or prsel == prtype:

        if prtype == None: new_string += f'-{prsel}'

        new_string = '.'.join( (new_string, str( prv )) )

config[ 'metadata' ][ 'version' ] = new_string

with open('setup.cfg', 'w') as configfile:
    config.write(configfile)

config.read( 'setup.cfg' )

print( config[ 'metadata' ][ 'version' ] )