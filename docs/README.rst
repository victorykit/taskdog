ezcfn
=====

A bootstrap for vanilla CloudFormation templating.

*ezcfn* is a CloudFormation bundling tool for keeping CloudFormation workflows 
vanilla-flavoured.

*ezcfn* simplifies working with vanilla CloudFormation templates. It takes care
of building, gathering and distributing resource artifacts (such as 
``Lambda Function`` code, or ``APIGateway`` specifications), without altering
the development workflow when working with static interchange format 
(*json* and *yaml*) templates.

*ezcfn* dissects and extends the existing functionalities of tools such as the
AWS CLI ``cloudformation package`` command.

Repository: `<https://bitbucket.org/victorykit/ezcfn>`_

Repository Mirror: `<https://github.com/victoryk-it/ezcfn>`_

.. include:: guide/get-started.rst

Documentation
-------------

The documentation can be found under `<https://victorykit.bitbucket.io/ezcfn/>`_.


Licensing
---------

Copyright (C) 2021  Tiara Rodney (victoryk.it)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

.. toctree::
    :maxdepth: 1
    :caption: More Information

    ARCHITECTURE
    CONTRIBUTING