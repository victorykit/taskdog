import os
import sys

sys.path.insert( 0, os.path.abspath( os.path.join( __file__, '..', '..', 'src' ) ) )

# -- Project information -------------------------------------------------------

project = 'ezcfn'
copyright = '2021 - Tiara Rodney (victoryk.it)'
author = 'Tiara Rodney <t.rodney@victoryk.it'

# The full version, including alpha/beta/rc tags
release = '1b'


# -- General configuration -----------------------------------------------------

extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.autosummary',
    'sphinx_rtd_theme'
]

templates_path = ['_templates']

exclude_patterns = [
    '_build',
    '_templates'
    'Thumbs.db',
    '.DS_Store',
    '.gitignore',
]


# -- Options for HTML output ---------------------------------------------------

html_theme = 'sphinx_rtd_theme'

#html_static_path = ['_static']


# -- Options for autodoc & autosummary -----------------------------------------

autodoc_default_options = {
    "private-members": True
}
autosummary_generate = True

if tags.has('readme'):
    autosummary_generate = False
    master_doc = 'README'
    exclude_patterns.append('index.rst')
    exclude_patterns.append('_stubs')
    exclude_patterns.append('guide')
