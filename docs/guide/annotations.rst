Annotations
===========

Annotations give `ezcfn` information on how to resolve a resource's artifacts.
Annotations are applied on a per-resource basis and define a 
:doc:`task <./tasks>` to be applied to the resource.

*ezcfn* supports two types of annotations: *intrinsic*, or *extrinsic*.

Intrinsic Annotations
---------------------

*Intrinsic* annotations are helpful, when defining CloudFormation 
templates adhering to the specifications/scheme. They can be linted with no 
interference.

Intrinsic annotations are done via providing a ``Metadata["ezcfn"]`` property.

Properties of resources resolved through *ezcfn* can either be omitted or 
supplied  with arbitrary values. In the example of an 
``AWS::ApiGatewayV2::API`` resource, it applies to ``BodyS3Location["Bucket"]`` 
and ``BodyS3Location["Key"]``. For more information, see 
:py:mod:`ezcfn.resolution.resolver`.

If no ``Metadata.ezcfn`` property is supplied, ``ezcfn`` will skip the 
evaluation of the resource.

The format of an extrinsic annotation is: ::

    Metadata:
        ezcfn:
            Type: <task-type>
            Path: <path>
            Options: <task-options>

.. code-block:: yaml
    :caption: Instrically Annotated Example

    AWSTemplateFormatVersion: "2010-09-09"
    Resources:
        Foobar2:
            Type: AWS::ApiGatewayV2::Api
            Properties: 
                BodyS3Location:
                    Bucket: "nil"
                    Key: "nil"
            Metadata:
                ezcfn:
                    Type: docker
                    Path: ./sample
                    Options:
                        artifact-path: "/ezcfn.zip"
                        build-arg:
                            FOO: bar

Extrinsic Annotations
---------------------

*Extrinsic* annotations are helpful, when defining slim CloudFormation 
templates. They however are not conformant to the CloudFormation 
specifications/scheme and may cause linting to fail.

Extrinsic annotation is done via providing a 
`RCF2396 <https://www.ietf.org/rfc/rfc2396.txt>`_ quasi-conformant URI string to 
the uppermost property defining the S3 object location.

The URI string is quasi-conformant, since the query portion may contain 
non-conformant characters.

The format of an extrinsic annotation is: ::

    <task-type>:<path>?<task-options>

.. code-block:: yaml
    :caption: Extrinsically Annotated Example

    AWSTemplateFormatVersion: "2010-09-09"
    Resources:
        Foobar:
            Type: AWS::ApiGatewayV2::Api
            Properties: 
                BodyS3Location: "docker:./sample?build-arg[FOO]=bar&artifact-path=/ezcfn.zip"