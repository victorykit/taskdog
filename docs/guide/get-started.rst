Get Started
===========

.. code-block:: shell

    python -m pip install ezcfn-victorykit
    ezcfn --help

.. code-block:: shell

    git clone git@bitbucket.org:victorykit/ezcfn-example.git
    cd ezcfn-example
    ezcfn resolve examples/nested/template.yaml \
        --s3-bucket mybucket \
        --s3-region eu-central-1 \
        --s3-prefix testd \
        --outdir ezcfn.out \
        --intrinsic
    ls -al ezcfn.out/testd