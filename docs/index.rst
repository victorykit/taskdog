.. include:: README.rst

.. autosummary::
    :toctree: _stubs
    :recursive:

    ezcfn

.. toctree::
    :maxdepth: 1
    :caption: Contents:

    guide/annotations
    guide/resolvers
    guide/tasks
    guide/get-started



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
