#!/usr/bin/env python3
"""Cloud Code Framework Robot

The ccfrobot (Cloud Code Framework Robot) is a programmable automaton for 
automating creative tasks that occur when working with the [Cloud Code 
Framework (CCF)](). It implements all CCF models, as well as skills, that 
are not destructive. In addition, it offers a CLI, as well as HTTP-REST 
APIs.

This is the CLI API.
"""