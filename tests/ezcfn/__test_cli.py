#!/usr/bin/env python3
import pytest, os



@pytest.fixture
def module():

    import ezcfn.cli as module

    return module


@pytest.fixture
def subparsersModule():

    import ezcfn.cli.subparsers as module

    return module



class Test_TemporaryImportPath():

    def test_default( self, module ):

        import sys

        basedirname = os.path.dirname( __file__ )

        dirname = os.path.join( basedirname, 'cli', 'mock' )

        with module._TemporaryImportPath( dirname ):

            import temporaryImportPath

            assert temporaryImportPath.FOO == 'bar'

            assert dirname in sys.path

        assert dirname not in sys.path


    def test_pathAlreadyExists( self, module ):

        import sys

        basedirname = os.path.dirname( __file__ )

        dirname = os.path.join( basedirname, 'cli', 'mock' )

        sys.path.insert( 0, dirname )

        with module._TemporaryImportPath( dirname ):

            import temporaryImportPath

            assert temporaryImportPath.FOO == 'bar'

            assert dirname in sys.path

        assert dirname in sys.path



class Test__getDefaultTopParser():

    def test_default( self, module ):

        import argparse

        parser = module._getDefaultTopParser()

        assert isinstance( parser, argparse.ArgumentParser )



class Test__getSubparsersByModuleName():

    def test_default( self, module, subparsersModule ):

        import sys, importlib

        basedirname = os.path.dirname( __file__ )

        dirname = os.path.join( basedirname, 'cli', 'mock', 'subparsers' )

        sys.path.insert( 0, dirname )

        predicate, objects = module._getSubparsersByModuleName( 'foo' )

        assert isinstance( predicate, subparsersModule.NonTerminalSubparser )

        assert len( objects ) == 1

        assert isinstance( objects[ 0 ], subparsersModule.TerminalSubparser )

        assert objects[ 0 ].ARGS == [ 'milkin' ]

        del sys.path[ 0 ]



class Test_getModuleParserObject():

    def test_default( self, module, subparsersModule ):

        import sys, importlib

        basedirname = os.path.dirname( __file__ )

        dirname = os.path.join( basedirname, 'cli', 'mock', 'subparsers' )

        sys.path.insert( 0, dirname )

        mod = importlib.import_module( 'foo' )

        predicate = module._getModuleParserObject( mod )

        assert isinstance( predicate, subparsersModule.NonTerminalSubparser )

        assert predicate.ARGS == [ 'foo' ]

        del sys.path[ 0 ]



class Test_getClassParserObjects():

    def test_default( self, module, subparsersModule ):

        import sys, importlib

        basedirname = os.path.dirname( __file__ )

        dirname = os.path.join( basedirname, 'cli', 'mock', 'subparsers' )

        sys.path.insert( 0, dirname )

        mod = importlib.import_module( 'foo' )

        objects = module._getClassParserObjects( mod )

        assert len( objects ) == 1

        assert isinstance( objects[ 0 ], subparsersModule.TerminalSubparser )

        assert objects[ 0 ].ARGS == [ 'milkin' ]

        del sys.path[ 0 ]



class Test_getSubparserByModulePath():
    
    def test_default( self, module ):

        basedirname = os.path.dirname( __file__ )

        path = os.path.join( basedirname, 'cli', 'mock', 'subparsers', 'foo.py' )

        predicate, objects = module._getSubparserByModulePath( path )



class Test_getSubparserModulesByPackagePath():

    def test_default( self, module ):

        basedirname = os.path.dirname( __file__ )

        dirname = os.path.join( basedirname, 'cli', 'mock', 'subparsers' )

        objs = module._getSubparserModulesByPackagePath( dirname )



class Test_upgradeParser():
    
    def test_default( self, module ):

        import argparse

        parser = argparse.ArgumentParser()

        pps = parser.add_subparsers( dest='test', required = True )

        obj = type( 'Object', (), {} )

        obj.ARGS = [ 'foo' ]

        obj.KWARGS = {}

        module._upgradeParser( pps, obj )



class Test_upgradeParserRecursive():

    def test_default( self, module ):

        import argparse

        parser = argparse.ArgumentParser()

        basedirname = os.path.dirname( __file__ )

        dirname = os.path.join( basedirname, 'cli', 'mock', 'subparsers' )

        module._upgradeParserRecursive( parser, dirname )



class Test__convertSnakeCaseToKebabCase:

    def test_default( self, module ):

        text = 'foo_bar'

        result = module._convertSnakeCaseToKebabCase( text )

        assert 'foo-bar' == result


    def test_switchToUpperCase( self, module ):

        import string

        text = 'foo_bar'

        result = module._convertSnakeCaseToKebabCase( text, string.ascii_uppercase )

        assert 'FOO-BAR' == result


    def test_switchToLowerCase( self, module ):

        import string

        text = 'FOO_BAR'

        result = module._convertSnakeCaseToKebabCase( text, string.ascii_lowercase )

        assert 'foo-bar' == result


    def test_switchToLowerCaseFromMixed( self, module ):

        import string

        text = 'Foo_Bar'

        result = module._convertSnakeCaseToKebabCase( text, string.ascii_lowercase )

        assert 'foo-bar' == result



class Test__convertCamelCaseToKebabCase():

    def test_default( self, module ):

        text = 'FooBar'

        result = module._convertCamelCaseToKebabCase( text )

        assert 'foo-bar' == result


    def test_bumpyCase( self, module ):

        text = 'fooBar'

        result = module._convertCamelCaseToKebabCase( text )

        assert 'foo-bar' == result