#!/usr/bin/env python3
import sys, os

importPath = os.path.abspath( os.path.join( __file__, '..', '..', '..', '..', '..' ) )

sys.path.insert( 0, importPath )

from ezcfn.cli import subparsers



class Milkin( subparsers.TerminalSubparser ):

    def construct( self, parser ):

        pass