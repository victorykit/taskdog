#!/usr/bin/env python3
import pytest, os, asyncio



@pytest.fixture
def module():

    from ezcfn.resolution import tasks as module

    return module



@pytest.fixture
def sample_resolver():

    from ezcfn.resolution.resolver import api_gateway_v2

    return api_gateway_v2



@pytest.fixture
def mockBasepath():

    return os.path.join( os.path.dirname( __file__ ), '__mocks__' )



class Test__parse_uri_query_string():

    def test_default( self, module, mockBasepath ):

        query_string = 'foo=bar&foo2=bar2'

        assert module._parse_uri_query_string( query_string ) == {
            'foo': 'bar',
            'foo2': 'bar2'
        }


    def test_array( self, module, mockBasepath ):

        query_string = 'foo[]=bar&foo[]=bar2'

        assert module._parse_uri_query_string( query_string ) == {
            'foo': [
                'bar',
                'bar2'
            ]
        }




class Test__parse_uri():

    def test_default( self, module, mockBasepath ):

        uri = 'file:./foo/bar'

        assert module._parse_uri( uri ) == ( 'file', './foo/bar', {} )


    def test_with_query( self, module, mockBasepath ):

        uri = 'file:./foo/bar?foo=bar'

        assert module._parse_uri( uri ) == (
            'file',
            './foo/bar',
            {
                'foo': 'bar'
            }
        )


    def test_with_query_array( self, module, mockBasepath ):

        uri = 'file:./foo/bar?foo[]=bar&foo[]=bar2'

        assert module._parse_uri( uri ) == (
            'file',
            './foo/bar',
            {
                'foo': [
                    'bar',
                    'bar2'
                ]
            }
        )



class Test__map_tuples_to_dict():

    def test_singleton( self, module, mockBasepath ):

        #this is pretty fugly, but thats how singletons work in Python...
        item0 = ( 'foo', 'bar' ),
        _tuple = tuple( item0 )

        assert module._map_tuples_to_dict( _tuple ) == {
            'foo': 'bar'
        }


    def test_multi( self, module, mockBasepath ):

        _tuple = ( ( 'foo', 'bar' ), ( 'foo2', 'bar2' ) )

        assert module._map_tuples_to_dict( _tuple ) == {
            'foo': 'bar',
            'foo2': 'bar2'
        }



class Test__load_json():

    def test_default( self, module, mockBasepath ):

        path = os.path.join( mockBasepath, 'sample.json' )

        assert module._load_json( path ) == {
            'foo': 'bar'
        }



class Test__load_yaml():

    def test_default( self, module, mockBasepath ):

        path = os.path.join( mockBasepath, 'sample.yaml' )

        assert module._load_yaml( path ) == {
            'foo': 'bar'
        }



class Test__input_interoperability__():

    def test_yaml_shorthand( self, module, mockBasepath ):

        path = os.path.join( mockBasepath, 'sample1.yaml' )
        path1 = os.path.join( mockBasepath, 'sample1-shorthand.yaml' )

        assert module._load_yaml( path ) == module._load_yaml( path1 )


    def test_yaml_and_json( self, module, mockBasepath ):

        path = os.path.join( mockBasepath, 'sample1.json' )
        path1 = os.path.join( mockBasepath, 'sample1.yaml' )

        assert module._load_json( path ) == module._load_yaml( path1 )



class Test__get_resolver():

    def test_default( self, module, mockBasepath ):

        node = {
            'Type': 'AWS::ApiGatewayV2::Api',
            'Properties': {}
        }

        assert module._get_resolver( 'foo', node, {} ) == ( None, None )


    def test_unresolved( self, module, mockBasepath, sample_resolver ):

        node = {
            'Type': 'AWS::ApiGatewayV2::Api',
            'Properties': {
                'BodyS3Location': 'file:./sample.txt'
            }
        }

        resolver_func, coro = module._get_resolver( 'foo', node, {} )

        assert asyncio.iscoroutine( coro )

        assert resolver_func == sample_resolver.ApiResolver.resolve



class Test__create_file_task():

    def test_default( self, module, mockBasepath ):

        data = [ 'foo', './foo', {}, {} ]
        assert asyncio.iscoroutine( module._create_file_task( *data ) )



class Test__create_docker_task():

    def test_default( self, module, mockBasepath ):

        data = [ 'foo', './foo', {}, {} ]
        assert asyncio.iscoroutine( module._create_docker_task( *data ) )



class Test__create_http_task():

    def test_default( self, module, mockBasepath ):

        data = [ 'foo', './foo', {}, {} ]
        assert asyncio.iscoroutine( module._create_docker_task( *data ) )



class Test__create_script_task():

    def test_default( self, module, mockBasepath ):

        data = [ 'foo', './foo', {}, {} ]
        assert asyncio.iscoroutine( module._create_script_task( *data ) )



class Test__create_pip_task():

    def test_default( self, module, mockBasepath ):

        data = [ 'foo', './foo', {}, {} ]
        assert asyncio.iscoroutine( module._create_pip_task( *data ) )



@pytest.mark.asyncio
class Test_copy_file():

    async def test_default( self, module, mockBasepath ):

        data = [
            'foo',
            os.path.join( mockBasepath, 'tasks', 'create_file', 'sample.txt' ),
            {},
            {
                'outdir': 'ezcfn.out'
            }
        ]

        result = await module.copy_file( *data )
        assert  result == ( 'foo', 'copy_file.sample' )



@pytest.mark.asyncio
class Test_execute_docker_build():

    async def test_default( self, module, mockBasepath ):

        data = [
            'foo',
            os.path.join( mockBasepath, 'tasks', 'docker_build' ),
            {},
            {
                'outdir': 'ezcfn.out'
            }
        ]

        result = await module.execute_docker_build( *data )
        assert result == ( 'foo', 'execute_docker_build.sample' )



@pytest.mark.asyncio
class Test_download_file():

    async def test_default( self, module, mockBasepath ):

        data = [
            'foo',
            'https://aws.amazon.com/robots.txt',
            {},
            {
                'outdir': 'ezcfn.out'
            }
        ]

        result = await module.download_file( *data )
        assert result == ( 'foo', 'download_file.sample' )



@pytest.mark.asyncio
class Test_execute_script():

    async def test_default( self, module, mockBasepath ):

        data = [
            'foo',
            os.path.join( mockBasepath, 'tasks', 'execute_script', 'script.sh' ),
            {},
            {
                'outdir': 'ezcfn.out'
            }
        ]

        result = await module.execute_script( *data )
        assert result == ( 'foo', 'execute_script.sample' )



@pytest.mark.asyncio
class Test_build_pypi_package():

    async def test_default( self, module, mockBasepath ):

        data = [
            'foo',
            os.path.join( mockBasepath, 'tasks', 'build_pypi_package', 'sample' ),
            {},
            {
                'outdir': 'ezcfn.out'
            }
        ]

        result = await module.build_pypi_package( *data )
        assert result == ( 'foo', 'build_pypi_package.sample' )



@pytest.mark.asyncio
class Test_resolve_template():

    async def test_default( self, module, mockBasepath ):

        data = [
            None,
            os.path.join( mockBasepath, 'tasks', 'resolve_template', 'sample.yaml' ),
            {},
            {
                'outdir': 'ezcfn.out',
                's3': {
                    'bucket': 'foobarbucket'
                }
            }
        ]

        result = await module.resolve_template( *data )
        assert result == ( None, 'ezcfn.out/sample.yaml' )