#!/usr/bin/env python3
import configparser

config = configparser.ConfigParser()

config.read( 'setup.cfg' )

print( config[ 'metadata' ][ 'version' ] )